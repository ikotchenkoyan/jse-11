package ru.t1.kotchenko.tm.api.controller;

public interface ICommandController {

    void showWelcome();

    void showVersion();

    void showHelp();

    void showArguments();

    void showCommands();

    void showDeveloperInfo();

    void showArgumentError();

    void showCommandError();

}
